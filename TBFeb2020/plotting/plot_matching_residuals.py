#!/usr/bin/env python

import sys
import os
import os.path as op

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
#import scipy.spatial
import ROOT
import root_numpy as rn
from lmfit import Model
from lmfit.models import ConstantModel, LinearModel
from scipy.special import erfc

unit_length = 'mm'
redchi2_max = 10.0
delta_uv_max = 0.250

# common plot settings
style = {
    'bins': 100,
    'range': (-delta_uv_max, delta_uv_max),
    'histtype': 'step',
    'log': False }
# initial fit parameter values
init_params = {
    'center': 0,
    'sigma': 0.050,
    'width': 0.070,
    'amplitude': 100.0 }

def plot(run, tel):
    # load data
    in_filename = '/afs/cern.ch/work/b/bdong/proteus/proteus/TBFeb2020/output/run{:06d}/recon_dut{}_straight4d-trees.root'.format(run,tel)
    tree_name = 'Tel{}/tracks_clusters_matched'.format(tel)
    f = ROOT.TFile(in_filename,"READ")
    if f.IsZombie():
        print ("Cannot open file {}.  Exiting.")
        sys.exit(1)
    tree_mat = f.Get(tree_name)
    if not tree_mat:
        print ("Cannot get tree {} from file {}.  Exiting.")
        sys.exit(1)

    # perform matching cut
    branches = 'trk_u trk_v trk_std_u trk_std_v trk_col trk_row clu_col clu_row clu_u clu_v clu_std_u clu_std_v clu_size clu_time'.split()
    selection = 'trk_size>=2 && (trk_chi2/trk_dof) < {0} && fabs(clu_u - trk_u) < {1} && fabs(clu_v - trk_v) < {1}'.format(redchi2_max, delta_uv_max)
    mat = rn.tree2array(tree_mat, branches=branches, selection=selection)

    # calculate residuals and pulls
    res_u = mat['clu_u'] - mat['trk_u']
    res_v = mat['clu_v'] - mat['trk_v']
    std_u = np.hypot(mat['trk_std_u'], mat['clu_std_u'])
    std_v = np.hypot(mat['trk_std_v'], mat['clu_std_v'])
    pull_u = res_u / std_u
    pull_v = res_v / std_v

    plot_input_dist(run, tel, 'matched_clu_cr', 'Matched cluster', mat['clu_col'], mat['clu_row'])
    plot_input_dist(run, tel, 'matched_trk_cr', 'Matched track', mat['trk_col'], mat['trk_row'])
    plot_residuals(run, tel, res_u, res_v)
    plot_time(run, tel, mat['clu_time'])
#    plt.show()

# define residuals model function
def smeared_box(x, center, sigma, width, amplitude):
    left = (x - center + 0.5 * width) / (np.sqrt(2.0) * sigma)
    right = (x - center - 0.5 * width) / (np.sqrt(2.0) * sigma)
    return 0.5 * amplitude * erfc(-left) * erfc(right)

def save(figure, plotname, *args, **kw):
    plotname = plotname.format(*args, **kw)
    dirname = op.dirname(plotname)
    if not op.isdir(dirname):
            os.makedirs(dirname)
    figure.savefig(plotname)
    print('saved \'{}\''.format(plotname))

def plot_input_dist(run, tel, name, title, col, row):
    fg, ax = plt.subplots()
    n, ex, ey, img = ax.hist2d(col, row, range=((1,80),(1,336)),bins=(80,336),cmin=1)
    ax.set_xlabel('{} column position'.format(title))
    ax.set_ylabel('{} row position'.format(title))
    fg.colorbar(img, label='Entries')
    save(fg,'plots/dist/run{:06d}-tel{}-{}.pdf',run,tel,name)

def plot_residuals(run, tel, res_u, res_v):
    fg, axs = plt.subplots(ncols=2, sharex=True)
    for ax, res in zip(axs, [res_u, res_v]):
        y, binedges, _ = ax.hist(res, **style)
        # bin edges to center positions
        x = 0.5 * (binedges[:-1] + binedges[1:])
        # use poission statistics to estimate weights
        w = 1 / np.sqrt(y)
        w[y == 0] = 0
        # fit w/ residual model
        model = Model(smeared_box, name='res') + ConstantModel(prefix='bkg')
        result = model.fit(y, x=x, weights=w, **init_params)
        print(result.fit_report())
        # compute relative fractions
        binwidths = binedges[1:] - binedges[:-1]
        components = result.eval_components()
        sum_res = np.sum(binwidths * components['res'])
        sum_bkg = np.sum(binwidths * components['bkg'])
        f_bkg = sum_bkg / (sum_res + sum_bkg)
        print('model sum background {}'.format(sum_bkg))
        print('model sum residuals {}'.format(sum_res))
        # plot fit components
        print(components['bkg'])
        #ax.plot(x, components['bkg'], ls='--', c='0.75')
        ax.axhline(components['bkg'], ls='--', c='0.75')
        ax.plot(x, components['res'], ls='--', c='0.75')
        # plot combined fit
        ax.plot(x, result.best_fit, '--')
        # add fit results
        txt = [
            'u = {:.2f} {}'.format(result.best_values['center'], unit_length),
            '$\\sigma$ = {:.2f} {}'.format(abs(result.best_values['sigma']), unit_length),
            'l = {:.2f} {}'.format(abs(result.best_values['width']), unit_length),
            '$f_{{bkg}}$ = {:.2g}'.format(f_bkg) ]
        ax.text(0.05, 0.95, '\n'.join(txt),
                transform=ax.transAxes,
                horizontalalignment='left',
                verticalalignment='top')
        # ax.autoscale(axis='x', tight=True)
        ax.set_xlim(-delta_uv_max, delta_uv_max)
        ax.set_ylim(ymin=1)

    axs[0].set_xlabel('U residuals / {}'.format(unit_length), fontsize=12)
    axs[0].set_ylabel('Entries', fontsize=12)
    # axs[0].set_ylim([0,1000000])
    axs[1].set_xlabel('V Residuals / {}'.format(unit_length), fontsize=12)
    axs[1].yaxis.set_label_position('right')
    axs[1].yaxis.set_ticks_position('right')
    save(fg,'plots/matching/run{:06d}-tel{}-match_res.pdf',run,tel)

def plot_time(run, tel, time):
    fg, ax = plt.subplots()
    ax.hist(time, range=(0, 16), bins=32)
    ax.set_xlabel('ATLASPix1 hit timestamp1 / 62.5 ns')
    save(fg, 'plots/matching/run{:06d}-tel{}-time.pdf',run,tel)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print ("Usage: {} <run #> <tel# as DUT>".format(sys.argv[0]))
        sys.exit(0)
    plot(int(sys.argv[1]),int(sys.argv[2]))
