#!/usr/bin/env python3

import os
import sys
import ROOT

Simulation = True

def plot(run,tel):
	mat_tree_name = 'Tel{}/tracks_clusters_matched'.format(tel)
	if Simulation:
		in_filename = '/afs/cern.ch/work/b/bdong/proteus/proteus/Simulation/output/recon_dut{}_straight4d-trees.root'.format(tel)
	else:
		in_filename = '/eos/user/b/bdong/TestBeamFeb2020_output/run{}_sixplane/recon_dut{}_straight4d-trees.root'.format(run,tel)
	print(in_filename)
	print(mat_tree_name)
	f = ROOT.TFile(in_filename,"READ")
	tree_mat = f.Get(mat_tree_name)
	#plot_tot(tree_mat, run, tel)
	plot_cluster_size_x(tree_mat, run, tel)
	plot_cluster_size_y(tree_mat, run, tel)

def plot_tot(tree_mat, run, tel):
	c = ROOT.TCanvas()
	tot = ROOT.TH1D('tot', '', 15, 0, 15)
	tot.SetStats(0)
	tree_mat.Draw('hit_value>>tot')
	tot.GetXaxis().SetTitle('tot')
	tot.GetYaxis().SetTitle('count')
	tot.Draw()
	pt = ROOT.TPaveText(0.75, 0.75, 0.85, 0.85, "brNDC")
	pt.SetFillStyle(0)
	pt.SetBorderSize(0)
	pt.SetTextAlign(12)
	pt.SetTextSize(0.03)
	pt.AddText("Mean = %.3f"%(tot.GetMean()))
	pt.AddText("RMS = %.3f"%(tot.GetRMS()))
	pt.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.75, 0.25, 0.9, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	if Simulation:
		pt1.AddText("Simulation")
	else:
		pt1.AddText("FTB data 2020")
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(tel))
	pt1.Draw("same")
	if Simulation:
		c.SaveAs('plots_sixplane/Simulation/tot/dut{}-Tel{}_tot.pdf'.format(tel, tel))
		c.SaveAs('plots_sixplane/Simulation/tot/dut{}-Tel{}_tot.C'.format(tel, tel))
	else:
		c.SaveAs('plots_sixplane/tot/run{}-dut{}-Tel{}_tot.pdf'.format(run, tel, tel))
		c.SaveAs('plots_sixplane/tot/run{}-dut{}-Tel{}_tot.C'.format(run, tel, tel))

def plot_cluster_size_x(tree_mat, run, tel):
	clusterSizeX = ROOT.TH1D('clusterSizeX','',10, 0, 10)
	clusterSizeX.SetStats(0)
	tree_mat.Draw('clu_size_col>>clusterSizeX')
	clusterSizeX.GetYaxis().SetRangeUser(0,1.5*clusterSizeX.GetMaximum())
	clusterSizeX.GetXaxis().SetTitle('cluster size in x directory [# of pixels]')
	clusterSizeX.GetYaxis().SetTitle('count')
	c = ROOT.TCanvas()
	clusterSizeX.Draw()
	pt = ROOT.TPaveText(0.75, 0.75, 0.85, 0.85, "brNDC")
	pt.SetFillStyle(0)
	pt.SetBorderSize(0)
	pt.SetTextAlign(12)
	pt.SetTextSize(0.03)
	pt.AddText("Mean = %.3f"%(clusterSizeX.GetMean()))
	pt.AddText("RMS = %.3f"%(clusterSizeX.GetRMS()))
	pt.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.75, 0.25, 0.9, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	if Simulation:
		pt1.AddText("Simulation")
	else:
		pt1.AddText("FTB data 2020")
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(tel))
	pt1.Draw("same")
	if Simulation:
		c.SaveAs('plots_sixplane/Simulation/clusterSize/dut{}-Tel{}-cluster_x.pdf'.format(tel, tel))
		c.SaveAs('plots_sixplane/Simulation/clusterSize/dut{}-Tel{}-cluster_x.C'.format(tel, tel))
	else:
		c.SaveAs('plots_sixplane/clusterSize/run{}-dut{}-Tel{}-cluster_x.pdf'.format(run, tel, tel))
		c.SaveAs('plots_sixplane/clusterSize/run{}-dut{}-Tel{}-cluster_x.C'.format(run, tel, tel))

def plot_cluster_size_y(tree_mat, run, tel):
	clusterSizeY = ROOT.TH1D('clusterSizeY','',10, 0, 10)
	clusterSizeY.SetStats(0)
	tree_mat.Draw('clu_size_row>>clusterSizeY')
	clusterSizeY.GetYaxis().SetRangeUser(0,1.5*clusterSizeY.GetMaximum())
	clusterSizeY.GetXaxis().SetTitle('cluster size in y directory [# of pixels]')
	clusterSizeY.GetYaxis().SetTitle('count')
	c = ROOT.TCanvas()
	clusterSizeY.Draw()
	pt = ROOT.TPaveText(0.75, 0.75, 0.85, 0.85, "brNDC")
	pt.SetFillStyle(0)
	pt.SetBorderSize(0)
	pt.SetTextAlign(12)
	pt.SetTextSize(0.03)
	pt.AddText("Mean = %.3f"%(clusterSizeY.GetMean()))
	pt.AddText("RMS = %.3f"%(clusterSizeY.GetRMS()))
	pt.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.75, 0.25, 0.9, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	if Simulation:
		pt1.AddText("Simulation")
	else:
		pt1.AddText("FTB data 2020")
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(tel))
	pt1.Draw("same")
	if Simulation:
		c.SaveAs('plots_sixplane/Simulation/clusterSize/dut{}-Tel{}-cluster_y.pdf'.format( tel, tel))
		c.SaveAs('plots_sixplane/Simulation/clusterSize/dut{}-Tel{}-cluster_y.C'.format( tel, tel))
	else:
		c.SaveAs('plots_sixplane/clusterSize/run{}-dut{}-Tel{}-cluster_y.pdf'.format(run, tel, tel))
		c.SaveAs('plots_sixplane/clusterSize/run{}-dut{}-Tel{}-cluster_y.C'.format(run, tel, tel))

if __name__ == '__main__':
    plot(sys.argv[1],int(sys.argv[2]))
