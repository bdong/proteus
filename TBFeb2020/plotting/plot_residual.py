#!/usr/bin/env python3

import os
import sys
import ROOT

Simulation = True

def tran(his):
	his_new = ROOT.TH1D("his_new", "", his.GetNbinsX(), his.GetXaxis().GetXmin()*1000,  his.GetXaxis().GetXmax()*1000)
	for i in range(his.GetNbinsX()):
		x_low = his.GetBinCenter(i)*1000
		his_new.Fill(x_low, his.GetBinContent(i))
	return his_new

def plot(run,tel, Tel):
	if Simulation:
		in_filename = '/afs/cern.ch/work/b/bdong/proteus/proteus/Simulation/output/recon_dut{}_straight4d-hists.root'.format(tel)
	else:
		in_filename = '/eos/user/b/bdong/TestBeamFeb2020_output/run{}_sixplane/recon_dut{}_straight4d-hists.root'.format(run,tel)
	f = ROOT.TFile(in_filename,"READ")
	plot_residual_u(f, run, tel, Tel)
	plot_residual_v(f, run, tel, Tel)

def plot_residual_u(f, run, tel, Tel):
	if tel == Tel:
		hs_x = f.Get('sensors/Tel{}/matching/res_u'.format(Tel))
	else:
		hs_x = f.Get('sensors/Tel{}/residuals/res_u'.format(Tel))
	hs_x = tran(hs_x)
	hs_x.SetStats(0)
	hs_x.GetXaxis().SetTitle('Cluster - track position u [ \mum]')
	hs_x.GetYaxis().SetTitleOffset(1.4)
	hs_x.GetYaxis().SetTitle('Events')
	c = ROOT.TCanvas()
	hs_x.Draw("hist")
	pt = ROOT.TPaveText(0.65, 0.75, 0.85, 0.85, "brNDC")
	pt.SetFillStyle(0)
	pt.SetBorderSize(0)
	pt.SetTextAlign(12)
	pt.SetTextSize(0.03)
	pt.AddText("Mean = %.4f \mum"%(hs_x.GetMean()))
	pt.AddText("RMS = %.4f \mum"%(hs_x.GetRMS()))
	pt.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.75, 0.25, 0.9, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	if Simulation:
		pt1.AddText("Simulation")
	else:
		pt1.AddText("FTB data 2020")
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(Tel))
	pt1.Draw("same")
	if Simulation:
		c.SaveAs('plots_sixplane/Simulation/residual/dut{}-Tel{}_u.pdf'.format(tel,Tel))
		c.SaveAs('plots_sixplane/Simulation/residual/dut{}-Tel{}_u.C'.format(tel,Tel))
	else:
		c.SaveAs('plots_sixplane/residual/run{}-dut{}-Tel{}_u.pdf'.format(run,tel,Tel))
		c.SaveAs('plots_sixplane/residual/run{}-dut{}-Tel{}_u.C'.format(run,tel,Tel))


def plot_residual_v(f, run, tel, Tel):
	if tel == Tel:
		hs_x = f.Get('sensors/Tel{}/matching/res_v'.format(Tel))
	else:
		hs_x = f.Get('sensors/Tel{}/residuals/res_v'.format(Tel))
	hs_x = tran(hs_x)
	hs_x.SetStats(0)
	hs_x.GetXaxis().SetTitle('Cluster - track position v [ \mum]')
	hs_x.GetYaxis().SetTitleOffset(1.4)
	hs_x.GetYaxis().SetTitle('Events')
	c = ROOT.TCanvas()
	hs_x.Draw("hist")
	pt = ROOT.TPaveText(0.65, 0.75, 0.85, 0.85, "brNDC")
	pt.SetFillStyle(0)
	pt.SetBorderSize(0)
	pt.SetTextAlign(12)
	pt.SetTextSize(0.03)
	pt.AddText("Mean = %.4f \mum"%(hs_x.GetMean()))
	pt.AddText("RMS = %.4f \mum"%(hs_x.GetRMS()))
	pt.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.75, 0.25, 0.9, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	if Simulation:
		pt1.AddText("Simulation")
	else:
		pt1.AddText("FTB data 2020")
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(Tel))
	pt1.Draw("same")
	if Simulation:
		c.SaveAs('plots_sixplane/Simulation/residual/dut{}-Tel{}_v.pdf'.format(tel,Tel))
		c.SaveAs('plots_sixplane/Simulation/residual/dut{}-Tel{}_v.C'.format(tel,Tel))
	else:
		c.SaveAs('plots_sixplane/residual/run{}-dut{}-Tel{}_v.pdf'.format(run,tel,Tel))
		c.SaveAs('plots_sixplane/residual/run{}-dut{}-Tel{}_v.C'.format(run,tel,Tel))


if __name__ == '__main__':
    plot(sys.argv[1],int(sys.argv[2]),int(sys.argv[3]))
