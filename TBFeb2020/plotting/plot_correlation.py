#!/usr/bin/env python2

import os
import sys
import ROOT

def plot(run,tel, Tel1, Tel2):
    in_filename = '/Users/dongbinbin/Desktop/work/ATLAS/Testbeam/Feb2020/sixplane/run{}_sixplane/recon_dut{}_straight4d-hists.root'.format(run,tel)
    f = ROOT.TFile(in_filename,"READ")
    plot_correlation_x(f, run, tel, Tel1, Tel2)
    plot_correlation_y(f, run, tel, Tel1, Tel2)

def plot_correlation_x(f, run, tel, Tel1, Tel2):
	hs_x = f.Get('correlations/Tel{}-Tel{}/correlation_x'.format(Tel1,Tel2))
	hs_x.GetYaxis().SetTitleOffset(0.8)
	hs_x.GetXaxis().SetTitle('Tel{} cluster position [mm]'.format(Tel1))
	hs_x.GetYaxis().SetTitle('Tel{} cluster position [mm]'.format(Tel2))
	hs_x.SetStats(False)
	c = ROOT.TCanvas()
	hs_x.Draw("colz")
	c.SaveAs('plots_sixplane/correlation/run{}/run{}-dut{}-Tel{}_vs_Tel{}_x.pdf'.format(run, run,tel,Tel1,Tel2))
def plot_correlation_y(f, run, tel, Tel1, Tel2):
	hs_x = f.Get('correlations/Tel{}-Tel{}/correlation_y'.format(Tel1,Tel2))
	hs_x.GetYaxis().SetTitleOffset(0.8)
	hs_x.GetXaxis().SetTitle('Tel{} cluster position [mm]'.format(Tel1))
	hs_x.GetYaxis().SetTitle('Tel{} cluster position [mm]'.format(Tel2))
	hs_x.SetStats(False)
	c = ROOT.TCanvas()
	hs_x.Draw("colz")
	c.SaveAs('plots_sixplane/correlation/run{}/run{}-dut{}-Tel{}_vs_Tel{}_y.pdf'.format(run, run, tel,Tel1,Tel2))

if __name__ == '__main__':
	plot(sys.argv[1],int(sys.argv[2]),int(sys.argv[3]),int(sys.argv[4]))
	#plot(int(sys.argv[1]),int(sys.argv[2]),int(sys.argv[3]))
