#!/usr/bin/env python2

import os
import sys
import ROOT

def plot(run,tel, Tel):
    in_filename = '/Users/dongbinbin/Desktop/work/ATLAS/Testbeam/Feb2020/sixplane/run{}_sixplane/recon_dut{}_straight4d-hists.root'.format(run,tel)
    f = ROOT.TFile(in_filename,"READ")
    plot_hit(f, run, tel, Tel)

def plot_hit(f, run, tel, Tel):
	hs_x = f.Get('sensors/Tel{}/hits/colrow'.format(Tel))
	hs_x.GetYaxis().SetTitleOffset(1.0)
	hs_x.GetXaxis().SetTitle('Tel{} Column'.format(Tel))
	hs_x.GetYaxis().SetTitle('Tel{} Row'.format(Tel))
	hs_x.SetStats(False)
	c = ROOT.TCanvas()
	hs_x.Draw("colz")
	c.SaveAs('plots_sixplane/hits/run{}/run{}-dut{}-Tel{}.pdf'.format(run, run,tel,Tel))

if __name__ == '__main__':
    plot(sys.argv[1],int(sys.argv[2]),int(sys.argv[3]))
