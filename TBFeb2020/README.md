Good run lists
=====================================

Info about good runs taken in Feb/Mar 2020 at Fermilab test beam.\
Info got from run log file [here](https://docs.google.com/spreadsheets/d/18C4pGnbw4ugC64ol_yFKco03sQbfG8ngIa5XkVRTyQo/edit#gid=1279585798)\
(with twisted angle on Tel10 and Tel12) Narrow beam data storage: /eos/user/b/bdong/TBFeb2020Data/ \
(with twisted angle on Tel10 and Tel12) Wide beam data storage: /eos/user/b/bdong/TBFeb2020\_wideBeam/\
(no twisted angle on Tel10 and Tel12) Narrow beam data storage: /eos/user/b/bdong/TBMar2020Data/NarrowBeam/ \
(no twisted angle on Tel10 and Tel12) Wide beam data storage: /eos/user/b/bdong/TBMar2020Data/WideBeam/ \
Run more than one run together -- use hadd to combine data from different run.

Runs collected with twisted angle on Tel10 and Tel12
=====================================================

Run 955
------------------
Tel 2,10,7,3,12,13\
Narrow beam\
No. of events around 390000

Run 963,964,967
---------------------
five out of six telescopes used. Tel 10,7,3,12,13 \
Narrow beam

Run 995
---------------------
Tel 5,10,7,3,12,13\
Narrow beam\
No. of events: 711k

Run 996, 997, 998, 999, 1000
--------------------------------
Tel 5,10,7,3,12,13 \
Narrow beam\
No. of events: 1M for each run

Run 1014, 1015, 1018
---------------------------
Tel 5,10,7,3,12,13\
Narrow beam\
No. of events: 1M for each run

Run 1032,1047,1048,1049,1050,1051,1052,1053,1054,1055,1056,1057,1058,1059,1060,1061,1062
------------------------
Tel 5,10,7,3,12,13\
Narrow beam\
No. of events: 1M for each run

Run 1068,1069,1070,1073,1074,1075,1076,1077,1078,1079,1080,1081,1082,1083,1084
--------------------------------------------
Tel 5,10,7,3,12,13\
Wide beam


Runs collected wighout twisted angle on Tel10 and Tel12
===========================================
Run 1174
-------------
Tel 5,10,7,3,12,13\
Narrow beam

Run 1175
--------------
Tel 5,10,7,3,12,13\
Narrow beam

Run 1176
---------------------
Tel 5,10,7,3,12,13\
Narrow beam

Run 1189, 1104, 1105, 1106
-----------------------------
Tel 5,10,7,3,12,13\
Wide beam
