#!/bin/bash
#runLists='001075'
#runLists='001068 001069 001070 001073 001074'
runLists='1076 1077 1078 1079 1080 1081 1082 1083 1084'
tels='5 10 7 3 12 13'

for run in $runLists
do
	for tel in $tels
	do
	export runNumber=${run}
	export telNumber=$tel
	mkdir /afs/cern.ch/work/b/bdong/proteus/proteus/TBFeb2020/output/run${run}
	pt-noisescan -v -c analysis.toml -d device.toml -g geometry.toml /eos/user/b/bdong/TBFeb2020_wideBeam/cosmic_${run}/cosmic_${run}.root output/run${run}/noisescan

	pt-align -v -c analysis.toml -d device.toml -g geometry.toml -u tel${tel}_coarse -m output/run${run}/noisescan-mask.toml /eos/user/b/bdong/TBFeb2020_wideBeam/cosmic_${run}/cosmic_${run}.root output/run${run}/align_tel${tel}_coarse

	pt-align -v -c analysis.toml -d device.toml -g output/run${run}/align_tel${tel}_coarse-geo.toml -u tel${tel}_fine -m output/run${run}/noisescan-mask.toml /eos/user/b/bdong/TBFeb2020_wideBeam/cosmic_${run}/cosmic_${run}.root output/run${run}/align_tel${tel}_fine

	pt-align -v -c analysis.toml -d device.toml -g output/run${run}/align_tel${tel}_fine-geo.toml -u dut${tel}_coarse -m output/run${run}/noisescan-mask.toml /eos/user/b/bdong/TBFeb2020_wideBeam/cosmic_${run}/cosmic_${run}.root output/run${run}/align_dut${tel}_coarse

	pt-align -v -c analysis.toml -d device.toml -g output/run${run}/align_dut${tel}_coarse-geo.toml -u dut${tel}_fine -m output/run${run}/noisescan-mask.toml /eos/user/b/bdong/TBFeb2020_wideBeam/cosmic_${run}/cosmic_${run}.root output/run${run}/align_dut${tel}_fine

#	pt-recon -v -c analysis.toml -d device.toml -g output/run${run}/align_dut${tel}_fine-geo.toml -u dut${tel}_gbl3d -m output/run${run}/noisescan-mask.toml /eos/user/b/bdong/TBFeb2020_wideBeam/cosmic_${run}/cosmic_${run}.root output/run${run}/recon_dut${tel}_gbl3d

	pt-recon -v -c analysis.toml -d device.toml -g output/run${run}/align_dut${tel}_fine-geo.toml -u dut${tel}_straight4d -m output/run${run}/noisescan-mask.toml /eos/user/b/bdong/TBFeb2020_wideBeam/cosmic_${run}/cosmic_${run}.root output/run${run}/recon_dut${tel}_straight4d
	done
done

