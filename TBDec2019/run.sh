pt-noisescan -v -c analysis.toml -d device.toml -g geometry.toml input/120GeV_protons.root output/noisescan

pt-align -v -c analysis.toml -d device.toml -g geometry.toml -u tel10_coarse -m output/noisescan-mask.toml input/120GeV_protons.root output/align_tel10_coarse

pt-align -v -c analysis.toml -d device.toml -g output/align_tel10_coarse-geo.toml -u tel10_fine -m output/noisescan-mask.toml input/120GeV_protons.root output/align_tel10_fine

pt-align -v -c analysis.toml -d device.toml -g output/align_tel10_fine-geo.toml -u dut10_coarse -m output/noisescan-mask.toml input/120GeV_protons.root output/align_dut10_coarse

pt-align -v -c analysis.toml -d device.toml -g output/align_dut10_coarse-geo.toml -u dut10_fine -m output/noisescan-mask.toml input/120GeV_protons.root output/align_dut10_fine

pt-recon -v -c analysis.toml -d device.toml -g output/align_dut10_fine-geo.toml -u dut10_gbl3d -m output/noisescan-mask.toml input/120GeV_protons.root output/recon_dut10_gbl3d

pt-recon -v -c analysis.toml -d device.toml -g output/align_dut10_fine-geo.toml -u dut10_straight4d -m output/noisescan-mask.toml input/120GeV_protons.root output/recon_dut10_straight4d
