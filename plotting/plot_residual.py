#!/usr/bin/env python3

import os
import sys
import ROOT

Simulation = False

def tran(his):
	his_new = ROOT.TH1D("his_new", "", his.GetNbinsX(), his.GetXaxis().GetXmin()*1000,  his.GetXaxis().GetXmax()*1000)
	for i in range(his.GetNbinsX()):
		x_low = his.GetBinCenter(i)*1000
		his_new.Fill(x_low, his.GetBinContent(i))
	return his_new

def plot(tel, Tel):
	if Simulation:
		in_filename = '/recon_dut{}_straight4d-hists.root'.format(tel)
	else:
		in_filename = '/eos/user/b/bdong/TestBeamFinal/dataInput/recon_dut{}_straight4d-hists.root'.format(tel)
	f = ROOT.TFile(in_filename,"READ")
	telCorr = 0
	TelCorr = 0
	if tel == 5:
		telCorr = 0
	elif tel == 10:
		telCorr = 1
	elif tel == 7:
		telCorr = 2
	elif tel == 3:
		telCorr = 3
	elif tel == 12:
		telCorr = 4
	elif tel == 13:
		telCorr = 5
	else:
		telCorr = tel


	if Tel == 5:
		TelCorr = 0
	elif Tel == 10:
		TelCorr = 1
	elif Tel == 7:
		TelCorr = 2
	elif Tel == 3:
		TelCorr = 3
	elif Tel == 12:
		TelCorr = 4
	elif Tel == 13:
		TelCorr = 5
	else:
		TelCorr = Tel

	plot_residual_u(f, tel, Tel, telCorr, TelCorr)
	plot_residual_v(f, tel, Tel, telCorr, TelCorr)

def plot_residual_u(f, tel, Tel, telCorr, TelCorr):
	if tel == Tel:
		hs_x = f.Get('sensors/Tel{}/matching/res_u'.format(Tel))
	else:
		hs_x = f.Get('sensors/Tel{}/residuals/res_u'.format(Tel))
	hs_x = tran(hs_x)
	hs_x.SetStats(0)
	hs_x.GetXaxis().SetTitle('Cluster - track position u [ \mum]')
	hs_x.GetYaxis().SetTitleOffset(1.4)
	hs_x.GetYaxis().SetTitle('Events')
	hs_x.SetLineWidth(2)
	hs_x.SetLineColor(1)
	c = ROOT.TCanvas()
	hs_x.Draw("hist")
	pt = ROOT.TPaveText(0.65, 0.75, 0.85, 0.85, "brNDC")
	pt.SetFillStyle(0)
	pt.SetBorderSize(0)
	pt.SetTextAlign(12)
	pt.SetTextSize(0.03)
	pt.AddText("Mean = %.4f \mum"%(hs_x.GetMean()))
	pt.AddText("RMS = %.4f \mum"%(hs_x.GetRMS()))
	pt.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.78, 0.25, 0.88, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	if Simulation:
		pt1.AddText("Simulation")
	else:
		pt1.AddText("FTB data 2020")
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(TelCorr))
	pt1.Draw("same")
	if Simulation:
		c.SaveAs('plots/residual/dut{}-Tel{}_u_simu.pdf'.format(telCorr,TelCorr))
		c.SaveAs('plots/residual/dut{}-Tel{}_u_simu.C'.format(telCorr,TelCorr))
	else:
		c.SaveAs('plots/residual/dut{}-Tel{}_u.pdf'.format(telCorr,TelCorr))
		c.SaveAs('plots/residual/dut{}-Tel{}_u.C'.format(telCorr,TelCorr))


def plot_residual_v(f, tel, Tel, telCorr, TelCorr):
	if tel == Tel:
		hs_x = f.Get('sensors/Tel{}/matching/res_v'.format(Tel))
	else:
		hs_x = f.Get('sensors/Tel{}/residuals/res_v'.format(Tel))
	hs_x = tran(hs_x)
	hs_x.SetStats(0)
	hs_x.GetXaxis().SetTitle('Cluster - track position v [ \mum]')
	hs_x.GetYaxis().SetTitleOffset(1.4)
	hs_x.GetYaxis().SetTitle('Events')
	hs_x.SetLineWidth(2)
	hs_x.SetLineColor(1)
	c = ROOT.TCanvas()
	hs_x.Draw("hist")
	pt = ROOT.TPaveText(0.65, 0.75, 0.85, 0.85, "brNDC")
	pt.SetFillStyle(0)
	pt.SetBorderSize(0)
	pt.SetTextAlign(12)
	pt.SetTextSize(0.03)
	pt.AddText("Mean = %.4f \mum"%(hs_x.GetMean()))
	pt.AddText("RMS = %.4f \mum"%(hs_x.GetRMS()))
	pt.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.78, 0.25, 0.88, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	if Simulation:
		pt1.AddText("Simulation")
	else:
		pt1.AddText("FTB data 2020")
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(TelCorr))
	pt1.Draw("same")
	if Simulation:
		c.SaveAs('plots/residual/dut{}-Tel{}_v_simu.pdf'.format(telCorr,TelCorr))
		c.SaveAs('plots/residual/dut{}-Tel{}_v_simu.C'.format(telCorr,TelCorr))
	else:
		c.SaveAs('plots/residual/dut{}-Tel{}_v.pdf'.format(telCorr,TelCorr))
		c.SaveAs('plots/residual/dut{}-Tel{}_v.C'.format(telCorr,TelCorr))


if __name__ == '__main__':
    plot(int(sys.argv[1]),int(sys.argv[2]))
