#!/usr/bin/env python3

import os
import sys
import ROOT

Simulation = False

def tran(his):
	his_new = ROOT.TH1D("his_new", "", his.GetNbinsX(), his.GetXaxis().GetXmin()*1000,  his.GetXaxis().GetXmax()*1000)
	for i in range(his.GetNbinsX()):
		x_low = his.GetBinCenter(i)*1000
		his_new.Fill(x_low, his.GetBinContent(i))
	return his_new

def plot(tel, Tel):
	simu_filename = '/eos/user/b/bdong/TestBeamFinal/simuInput/recon_dut{}_straight4d-hists.root'.format(tel)
	data_filename = '/eos/user/b/bdong/TestBeamFinal/dataInput/recon_dut{}_straight4d-hists.root'.format(tel)
	simu_f = ROOT.TFile(simu_filename,"READ")
	data_f = ROOT.TFile(data_filename,"READ")
	telCorr = 0
	TelCorr = 0
	if tel == 5:
		telCorr = 0
	elif tel == 10:
		telCorr = 1
	elif tel == 7:
		telCorr = 2
	elif tel == 3:
		telCorr = 3
	elif tel == 12:
		telCorr = 4
	elif tel == 13:
		telCorr = 5
	else:
		telCorr = tel


	if Tel == 5:
		TelCorr = 0
	elif Tel == 10:
		TelCorr = 1
	elif Tel == 7:
		TelCorr = 2
	elif Tel == 3:
		TelCorr = 3
	elif Tel == 12:
		TelCorr = 4
	elif Tel == 13:
		TelCorr = 5
	else:
		TelCorr = Tel

	plot_residual_u(simu_f, data_f, tel, Tel, telCorr, TelCorr)
	plot_residual_v(simu_f, data_f, tel, Tel, telCorr, TelCorr)

def plot_residual_u(simu_f, data_f, tel, Tel, telCorr, TelCorr):
	if tel == Tel:
		simu_hs = simu_f.Get('sensors/Tel{}/matching/res_u'.format(Tel))
		data_hs = data_f.Get('sensors/Tel{}/matching/res_u'.format(Tel))
	else:
		simu_hs = simu_f.Get('sensors/Tel{}/residuals/res_u'.format(Tel))
		data_hs = data_f.Get('sensors/Tel{}/residuals/res_u'.format(Tel))
	simu_hs = tran(simu_hs)
	data_hs = tran(data_hs)
	print(data_hs.Integral())
	print(simu_hs.Integral())
	simu_hs.Scale(1./simu_hs.Integral())
	data_hs.Scale(1./data_hs.Integral())
	simu_hs.SetStats(0)
	data_hs.SetStats(0)
	simu_hs.GetXaxis().SetTitle('Cluster - track position u [ \mum]')
	data_hs.GetXaxis().SetTitle('Cluster - track position u [ \mum]')
	simu_hs.GetYaxis().SetTitleOffset(1.4)
	#simu_hs.GetYaxis().SetTitle('Events')
	simu_hs.SetLineWidth(2)
	simu_hs.SetLineColor(1)
	data_hs.SetLineWidth(2)
	data_hs.SetLineColor(2)
	c = ROOT.TCanvas()
	simu_hs.Draw("hist")
	data_hs.Draw("histsame")
	leg = ROOT.TLegend(0.6, 0.7, 0.75, 0.8)
	leg.SetTextFont(62)
	leg.SetTextSize(0.03)
	leg.SetFillStyle(0)
	leg.SetBorderSize(0)
	leg.SetTextAlign(12)
	leg.AddEntry(simu_hs, "MC, RMS = %.2f \mum"%(simu_hs.GetRMS()))
	leg.AddEntry(data_hs, "data, RMS = %.2f \mum"%(data_hs.GetRMS()))
	leg.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.78, 0.25, 0.88, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(TelCorr))
	pt1.Draw("same")
	c.SaveAs('plots/residual/comp_dut{}-Tel{}_u.pdf'.format(telCorr,TelCorr))
	c.SaveAs('plots/residual/comp_dut{}-Tel{}_u.C'.format(telCorr,TelCorr))

def plot_residual_v(simu_f, data_f, tel, Tel, telCorr, TelCorr):
	if tel == Tel:
		simu_hs = simu_f.Get('sensors/Tel{}/matching/res_v'.format(Tel))
		data_hs = data_f.Get('sensors/Tel{}/matching/res_v'.format(Tel))
	else:
		simu_hs = simu_f.Get('sensors/Tel{}/residuals/res_v'.format(Tel))
		data_hs = data_f.Get('sensors/Tel{}/residuals/res_v'.format(Tel))
	simu_hs = tran(simu_hs)
	data_hs = tran(data_hs)
	print(data_hs.Integral())
	print(simu_hs.Integral())
	simu_hs.Scale(1./simu_hs.Integral())
	data_hs.Scale(1./data_hs.Integral())
	simu_hs.SetStats(0)
	data_hs.SetStats(0)
	simu_hs.GetXaxis().SetTitle('Cluster - track position v [ \mum]')
	data_hs.GetXaxis().SetTitle('Cluster - track position v [ \mum]')
	simu_hs.GetYaxis().SetTitleOffset(1.4)
	#simu_hs.GetYaxis().SetTitle('Events')
	simu_hs.SetLineWidth(2)
	simu_hs.SetLineColor(1)
	data_hs.SetLineWidth(2)
	data_hs.SetLineColor(2)
	c = ROOT.TCanvas()
	simu_hs.Draw("hist")
	data_hs.Draw("histsame")
	leg = ROOT.TLegend(0.6, 0.7, 0.75, 0.8)
	leg.SetTextFont(62)
	leg.SetTextSize(0.03)
	leg.SetFillStyle(0)
	leg.SetBorderSize(0)
	leg.SetTextAlign(12)
	leg.AddEntry(simu_hs, "MC, RMS = %.2f \mum"%(simu_hs.GetRMS()))
	leg.AddEntry(data_hs, "data, RMS = %.2f \mum"%(data_hs.GetRMS()))
	leg.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.78, 0.25, 0.88, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(TelCorr))
	pt1.Draw("same")
	c.SaveAs('plots/residual/comp_dut{}-Tel{}_v.pdf'.format(telCorr,TelCorr))
	c.SaveAs('plots/residual/comp_dut{}-Tel{}_v.C'.format(telCorr,TelCorr))


if __name__ == '__main__':
    plot(int(sys.argv[1]),int(sys.argv[2]))
