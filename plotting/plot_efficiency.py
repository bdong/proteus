#!/usr/bin/env python3

import os
import sys
import os.path as op
from operator import truediv

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import ROOT
import root_numpy as rn

dist_uv_max = 250.0
pitch_u = 250.0
pitch_v = 50.0
eff_min = 0.95

# common plot settings
roi = ((1, 80), (1, 336))
bins = (roi[0][1] - roi[0][0], roi[1][1] - roi[1][0])
aspect = pitch_v / pitch_u
relheight = (bins[1] * pitch_u) / (bins[0] * pitch_v)
style = {
    'bins': bins,
    'range': roi,
    'cmin': 1,
}

def plot(telCorr):

    tel = 0
    if telCorr == 5:
	    tel = 0
    elif telCorr == 10:
	    tel = 1
    elif telCorr == 7:
	    tel = 2
    elif telCorr == 3:
	    tel = 3
    elif telCorr == 12:
	    tel = 4
    elif telCorr == 13:
	    tel =5
    else:
	    print "Wrong input"
		
    in_filename = '/eos/user/b/bdong/TestBeamFinal/dataInput/recon_dut{}_straight4d-trees.root'.format(telCorr)
    mat_tree_name = 'Tel{}/tracks_clusters_matched'.format(telCorr)
    unm_tree_name = 'Tel{}/clusters_unmatched'.format(telCorr)
    f = ROOT.TFile(in_filename,"READ")
    if f.IsZombie():
        print "Cannot open file {}.  Exiting.".format(in_filename)
        sys.exit(1)
    tree_mat = f.Get(mat_tree_name)
    if not tree_mat:
        print "Cannot get tree {} from file {}.  Exiting.".format(mat_tree_name,in_filename)
        sys.exit(1)
    tree_unm = f.Get(unm_tree_name)
    if not tree_unm:
        print "Cannot get tree {} from file {}.  Exiting.".format(unm_tree_name,in_filename)
        sys.exit(1)

    # perform matching cut
    branches = 'trk_col trk_row clu_col clu_row'.split()
    roi_selection = '{}<=trk_col && trk_col<{} && {}<=trk_row && trk_row<{}'.format(*(roi[0] + roi[1]))
    ref_selection = roi_selection + ' && trk_size>=2 && (trk_chi2/trk_dof)<10'
    mat_selection = ref_selection + ' && 0<clu_size && fabs(clu_u - trk_u)<{0} && fabs(clu_v - trk_v)<{0}'.format(dist_uv_max)
#    ref = rn.tree2array(tree_mat, branches=branches)
#    mat = rn.tree2array(tree_mat, branches=branches)
    ref = rn.tree2array(tree_mat, branches=branches, selection=ref_selection)
    mat = rn.tree2array(tree_mat, branches=branches, selection=mat_selection)

    plot_efficiency(tel, ref['trk_col'], ref['trk_row'], mat['trk_col'], mat['trk_row'])
    #plot_efficiency_inpixel(tel, ref['trk_col'], ref['trk_row'], mat['trk_col'], mat['trk_row'])
    #plt.show()

def save(figure, plotname, *args, **kw):
    plotname = plotname.format(*args, **kw)
    dirname = op.dirname(plotname)
    if not op.isdir(dirname):
            os.makedirs(dirname)
    figure.savefig(plotname)
    print('saved \'{}\''.format(plotname))

def plot_input_dist(tel, name, title, col, row):
    fg, ax = plt.subplots()
    n, ex, ey, img = ax.hist2d(col, row, range=((-5, 30), (-5, 405)), bins=(35, 410), cmin=1)
    ax.set_xlabel('{} column position'.format(title))
    ax.set_ylabel('{} row position'.format(title))
    fg.colorbar(img, label='Entries')
    save(fg, 'plots/dist/tel{}-{}_sixplane.pdf', tel, name)

def plot_efficiency(tel, ref_col, ref_row, mat_col, mat_row):
    # plot efficiency
    fg_eff, ax_eff = plt.subplots()
    trk_ref, x, y = np.histogram2d(ref_col, ref_row, bins=bins, range=roi)
    trk_mat, _, _ = np.histogram2d(mat_col, mat_row, bins=bins, range=roi)
    eff = np.ma.masked_invalid(trk_mat / trk_ref)

    img = ax_eff.pcolormesh(x, y, eff.T, cmap='RdYlGn', vmin=eff_min, vmax=1.0)
    cbar = fg_eff.colorbar(img, ax=ax_eff, label='Efficiency')
    ax_eff.set_xlabel('Column position')
    ax_eff.set_ylabel('Row position')
    save(fg_eff, 'plots/eff/tel{}-eff.pdf', tel)

def plot_efficiency_inpixel(tel, ref_col, ref_row, mat_col, mat_row, npixels=2):
    inpix_ref_col = np.remainder(ref_col, npixels)
    inpix_ref_row = np.remainder(ref_row, npixels)
    inpix_mat_col = np.remainder(mat_col, npixels)
    inpix_mat_row = np.remainder(mat_row, npixels)

    kw = {
            'range': ((0, npixels), (0, npixels)),
            'bins': (int(pitch_u / 5.0), int(pitch_v / 5.0)),
    }
    ref, x, y = np.histogram2d(inpix_ref_col, inpix_ref_row, **kw)
    mat, _, _ = np.histogram2d(inpix_mat_col, inpix_mat_row, **kw)
    eff = np.ma.masked_invalid(mat / ref)

    fg, ax = plt.subplots()
    img = ax.pcolormesh(x, y, eff.T, cmap='RdYlGn', vmin=eff_min, vmax=1.0)
    cbar = fg.colorbar(img, ax=ax, label='In-pixel efficiency')
    ax.set_xlabel('In-pixel column position')
    ax.set_ylabel('In-pixel row position')

    save(fg, 'plots/eff/tel{}-eff_inpixel{}_sixplane.pdf', tel, npixels)

if __name__ == '__main__':
    plot(int(sys.argv[1]))
