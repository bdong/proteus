#!/usr/bin/env python3

import os
import sys
import ROOT

Simulation = False 

def plot(tel):
	mat_tree_name = 'Tel{}/tracks_clusters_matched'.format(tel)
	if Simulation:
		in_filename = '/eos/user/b/bdong/TestBeamFinal/simuInput/recon_dut{}_straight4d-trees.root'.format(tel)
	else:
		in_filename = '/eos/user/b/bdong/TestBeamFinal/dataInput/recon_dut{}_straight4d-trees.root'.format(tel)
	print(in_filename)
	print(mat_tree_name)
	telCorr = 0
	if tel == 5:
		telCorr = 0
	elif tel == 10:
		telCorr = 1
	elif tel == 7:
		telCorr = 2
	elif tel == 3:
		telCorr = 3
	elif tel == 12:
		telCorr = 4
	elif tel == 13:
		telCorr = 5
	else:
		telCorr = tel
	f = ROOT.TFile(in_filename,"READ")
	tree_mat = f.Get(mat_tree_name)
	#plot_tot(tree_mat, tel, telCorr)
	plot_cluster_size(tree_mat, tel, telCorr)
	plot_cluster_size_x(tree_mat, tel, telCorr)
	plot_cluster_size_y(tree_mat, tel, telCorr)

# simulation no need to plot tot
def plot_tot(tree_mat, tel, telCorr):
	c = ROOT.TCanvas()
	tot = ROOT.TH1D('tot', '', 15, 0, 15)
	tot.SetStats(0)
	tree_mat.Draw('hit_value>>tot')
	tot.GetXaxis().SetTitle('tot')
	tot.GetYaxis().SetTitle('count')
	tot.SetLineWidth(2)
	tot.SetLineColor(1)
	tot.Draw()
	pt = ROOT.TPaveText(0.7, 0.75, 0.8, 0.85, "brNDC")
	pt.SetFillStyle(0)
	pt.SetBorderSize(0)
	pt.SetTextAlign(12)
	pt.SetTextSize(0.03)
	pt.AddText("Mean = %.3f"%(tot.GetMean()))
	pt.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.78, 0.25, 0.88, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	if Simulation:
		pt1.AddText("Simulation")
	else:
		pt1.AddText("FTB data 2020")
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(telCorr))
	pt1.Draw("same")
	c.SaveAs('plots/tot/dut{}-Tel{}_tot.pdf'.format(telCorr, telCorr))
	c.SaveAs('plots/tot/dut{}-Tel{}_tot.C'.format(telCorr, telCorr))

def plot_cluster_size_x(tree_mat, tel, telCorr):
	clusterSizeX = ROOT.TH1D('clusterSizeX','',10, 0, 10)
	clusterSizeX.SetStats(0)
	tree_mat.Draw('clu_size_col>>clusterSizeX')
	clusterSizeX.GetYaxis().SetRangeUser(0,1.5*clusterSizeX.GetMaximum())
	clusterSizeX.GetXaxis().SetTitle('cluster size in x directory [# of pixels]')
	clusterSizeX.GetYaxis().SetTitle('count')
	clusterSizeX.SetLineWidth(2)
	clusterSizeX.SetLineColor(1)
	c = ROOT.TCanvas()
	clusterSizeX.Draw()
	pt = ROOT.TPaveText(0.7, 0.75, 0.8, 0.85, "brNDC")
	pt.SetFillStyle(0)
	pt.SetBorderSize(0)
	pt.SetTextAlign(12)
	pt.SetTextSize(0.03)
	pt.AddText("Mean = %.3f"%(clusterSizeX.GetMean()))
	pt.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.78, 0.25, 0.88, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	if Simulation:
		pt1.AddText("Simulation")
	else:
		pt1.AddText("FTB data 2020")
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(telCorr))
	pt1.Draw("same")
	if Simulation:
		c.SaveAs('plots/clusterSize/dut{}-tel{}-cluster_x_simu.pdf'.format(telCorr, telCorr))
		c.SaveAs('plots/clusterSize/dut{}-tel{}-cluster_x_simu.C'.format(telCorr, telCorr))
	else:
		c.SaveAs('plots/clusterSize/dut{}-tel{}-cluster_x.pdf'.format(telCorr, telCorr))
		c.SaveAs('plots/clusterSize/dut{}-tel{}-cluster_x.C'.format(telCorr, telCorr))

def plot_cluster_size_y(tree_mat, tel, telCorr):
	clusterSizeY = ROOT.TH1D('clusterSizeY','',10, 0, 10)
	clusterSizeY.SetStats(0)
	tree_mat.Draw('clu_size_row>>clusterSizeY')
	clusterSizeY.GetYaxis().SetRangeUser(0,1.5*clusterSizeY.GetMaximum())
	clusterSizeY.GetXaxis().SetTitle('cluster size in y directory [# of pixels]')
	clusterSizeY.GetYaxis().SetTitle('count')
	clusterSizeY.SetLineWidth(2)
	clusterSizeY.SetLineColor(1)
	c = ROOT.TCanvas()
	clusterSizeY.Draw()
	pt = ROOT.TPaveText(0.7, 0.75, 0.8, 0.85, "brNDC")
	pt.SetFillStyle(0)
	pt.SetBorderSize(0)
	pt.SetTextAlign(12)
	pt.SetTextSize(0.03)
	pt.AddText("Mean = %.3f"%(clusterSizeY.GetMean()))
	pt.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.78, 0.25, 0.88, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	if Simulation:
		pt1.AddText("Simulation")
	else:
		pt1.AddText("FTB data 2020")
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(telCorr))
	pt1.Draw("same")
	if Simulation:
		c.SaveAs('plots/clusterSize/dut{}-tel{}-cluster_y_simu.pdf'.format(telCorr, telCorr))
		c.SaveAs('plots/clusterSize/dut{}-tel{}-cluster_y_simu.C'.format( telCorr, telCorr))
	else:
		c.SaveAs('plots/clusterSize/dut{}-tel{}-cluster_y.pdf'.format(telCorr, telCorr))
		c.SaveAs('plots/clusterSize/dut{}-tel{}-cluster_y.C'.format(telCorr, telCorr))


def plot_cluster_size(tree_mat, tel, telCorr):
	clusterSize = ROOT.TH1D('clusterSize','',10, 0, 10)
	clusterSize.SetStats(0)
	tree_mat.Draw('clu_size_row>>clusterSize')
	clusterSize.GetYaxis().SetRangeUser(0,1.5*clusterSize.GetMaximum())
	clusterSize.GetXaxis().SetTitle('cluster size [# of pixels]')
	clusterSize.GetYaxis().SetTitle('count')
	clusterSize.SetLineWidth(2)
	clusterSize.SetLineColor(1)
	c = ROOT.TCanvas()
	clusterSize.Draw()
	pt = ROOT.TPaveText(0.7, 0.75, 0.8, 0.85, "brNDC")
	pt.SetFillStyle(0)
	pt.SetBorderSize(0)
	pt.SetTextAlign(12)
	pt.SetTextSize(0.03)
	pt.AddText("Mean = %.3f"%(clusterSize.GetMean()))
	pt.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.78, 0.25, 0.88, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	if Simulation:
		pt1.AddText("Simulation")
	else:
		pt1.AddText("FTB data 2020")
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(telCorr))
	pt1.Draw("same")
	if Simulation:
		c.SaveAs('plots/clusterSize/dut{}-tel{}-cluster_simu.pdf'.format(telCorr, telCorr))
		c.SaveAs('plots/clusterSize/dut{}-tel{}-cluster_simu.C'.format( telCorr, telCorr))
	else:
		c.SaveAs('plots/clusterSize/dut{}-tel{}-cluster.pdf'.format(telCorr, telCorr))
		c.SaveAs('plots/clusterSize/dut{}-tel{}-cluster.C'.format(telCorr, telCorr))


if __name__ == '__main__':
    plot(int(sys.argv[1]))
