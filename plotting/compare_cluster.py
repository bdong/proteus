#!/usr/bin/env python3

import os
import sys
import ROOT

def plot(tel):
	mat_tree_name = 'Tel{}/tracks_clusters_matched'.format(tel)
	simu_filename = '/eos/user/b/bdong/TestBeamFinal/simuInput/recon_dut{}_straight4d-trees.root'.format(tel)
	data_filename = '/eos/user/b/bdong/TestBeamFinal/dataInput/recon_dut{}_straight4d-trees.root'.format(tel)
	telCorr = 0
	if tel == 5:
		telCorr = 0
	elif tel == 10:
		telCorr = 1
	elif tel == 7:
		telCorr = 2
	elif tel == 3:
		telCorr = 3
	elif tel == 12:
		telCorr = 4
	elif tel == 13:
		telCorr = 5
	else:
		telCorr = tel
	simu_f= ROOT.TFile(simu_filename,"READ")
	data_f= ROOT.TFile(data_filename,"READ")
	simu_tree_mat = simu_f.Get(mat_tree_name)
	data_tree_mat = data_f.Get(mat_tree_name)
	plot_cluster_size(simu_tree_mat, data_tree_mat, tel, telCorr)
	plot_cluster_size_x(simu_tree_mat, data_tree_mat, tel, telCorr)
	plot_cluster_size_y(simu_tree_mat, data_tree_mat, tel, telCorr)


def plot_cluster_size_x(simu_tree_mat,data_tree_mat, tel, telCorr):
	simu_clusterSizeX = ROOT.TH1D('simu_clusterSizeX','',10, 0, 10)
	data_clusterSizeX = ROOT.TH1D('data_clusterSizeX','',10, 0, 10)
	simu_clusterSizeX.SetStats(0)
	data_clusterSizeX.SetStats(0)
	simu_tree_mat.Draw('clu_size_col>>simu_clusterSizeX')
	data_tree_mat.Draw('clu_size_col>>data_clusterSizeX')
	simu_clusterSizeX.Scale(1./simu_clusterSizeX.GetEntries())
	data_clusterSizeX.Scale(1./data_clusterSizeX.GetEntries())
	simu_clusterSizeX.GetYaxis().SetRangeUser(0,1.5*simu_clusterSizeX.GetMaximum())
	simu_clusterSizeX.GetXaxis().SetTitle('cluster size in x directory [# of pixels]')
	#clusterSizeX.GetYaxis().SetTitle('count')
	simu_clusterSizeX.SetLineWidth(2)
	simu_clusterSizeX.SetMarkerSize(0)
	simu_clusterSizeX.SetLineColor(1)
	simu_clusterSizeX.SetMarkerColor(1)
	data_clusterSizeX.SetLineWidth(2)
	data_clusterSizeX.SetMarkerSize(0)
	data_clusterSizeX.SetLineColor(2)
	data_clusterSizeX.SetMarkerColor(2)
	c = ROOT.TCanvas()
	legend = ROOT.TLegend(0.60, 0.7, 0.75, 0.8)
	legend.SetTextFont(62)
	legend.SetTextSize(0.03)
	legend.SetBorderSize(0)
	legend.SetFillStyle(0)
	legend.SetTextAlign(12)
	simu_clusterSizeX.Draw("hist")
	data_clusterSizeX.Draw("samehist")
	legend.AddEntry(simu_clusterSizeX, "MC, mean = %.2f \mum"%simu_clusterSizeX.GetMean(), "l")
	legend.AddEntry(data_clusterSizeX, "data, mean = %.2f \mum"%data_clusterSizeX.GetMean(), "l")
	legend.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.78, 0.25, 0.88, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(telCorr))
	pt1.Draw("same")
	c.SaveAs('plots/clusterSize/comp_dut{}-tel{}-cluster_x.pdf'.format(telCorr, telCorr))
	c.SaveAs('plots/clusterSize/comp_dut{}-tel{}-cluster_x.C'.format(telCorr, telCorr))

def plot_cluster_size_y(simu_tree_mat,data_tree_mat, tel, telCorr):
	simu_clusterSizeX = ROOT.TH1D('simu_clusterSizeX','',10, 0, 10)
	data_clusterSizeX = ROOT.TH1D('data_clusterSizeX','',10, 0, 10)
	simu_clusterSizeX.SetStats(0)
	data_clusterSizeX.SetStats(0)
	simu_tree_mat.Draw('clu_size_row>>simu_clusterSizeX')
	data_tree_mat.Draw('clu_size_row>>data_clusterSizeX')
	simu_clusterSizeX.Scale(1./simu_clusterSizeX.GetEntries())
	data_clusterSizeX.Scale(1./data_clusterSizeX.GetEntries())
	simu_clusterSizeX.GetYaxis().SetRangeUser(0,1.5*simu_clusterSizeX.GetMaximum())
	simu_clusterSizeX.GetXaxis().SetTitle('cluster size in y directory [# of pixels]')
	#clusterSizeX.GetYaxis().SetTitle('count')
	simu_clusterSizeX.SetLineWidth(2)
	simu_clusterSizeX.SetMarkerSize(0)
	simu_clusterSizeX.SetLineColor(1)
	simu_clusterSizeX.SetMarkerColor(1)
	data_clusterSizeX.SetLineWidth(2)
	data_clusterSizeX.SetMarkerSize(0)
	data_clusterSizeX.SetLineColor(2)
	data_clusterSizeX.SetMarkerColor(2)
	c = ROOT.TCanvas()
	legend = ROOT.TLegend(0.60, 0.7, 0.75, 0.8)
	legend.SetTextFont(62)
	legend.SetTextSize(0.03)
	legend.SetBorderSize(0)
	legend.SetFillStyle(0)
	legend.SetTextAlign(12)
	simu_clusterSizeX.Draw("hist")
	data_clusterSizeX.Draw("samehist")
	legend.AddEntry(simu_clusterSizeX, "MC, mean = %.2f \mum"%simu_clusterSizeX.GetMean(), "l")
	legend.AddEntry(data_clusterSizeX, "data, mean = %.2f \mum"%data_clusterSizeX.GetMean(), "l")
	legend.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.78, 0.25, 0.88, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(telCorr))
	pt1.Draw("same")
	c.SaveAs('plots/clusterSize/comp_dut{}-tel{}-cluster_y.pdf'.format(telCorr, telCorr))
	c.SaveAs('plots/clusterSize/comp_dut{}-tel{}-cluster_y.C'.format(telCorr, telCorr))

def plot_cluster_size(simu_tree_mat,data_tree_mat, tel, telCorr):
	simu_clusterSizeX = ROOT.TH1D('simu_clusterSizeX','',10, 0, 10)
	data_clusterSizeX = ROOT.TH1D('data_clusterSizeX','',10, 0, 10)
	simu_clusterSizeX.SetStats(0)
	data_clusterSizeX.SetStats(0)
	simu_tree_mat.Draw('clu_size>>simu_clusterSizeX')
	data_tree_mat.Draw('clu_size>>data_clusterSizeX')
	simu_clusterSizeX.Scale(1./simu_clusterSizeX.GetEntries())
	data_clusterSizeX.Scale(1./data_clusterSizeX.GetEntries())
	simu_clusterSizeX.GetYaxis().SetRangeUser(0,1.5*simu_clusterSizeX.GetMaximum())
	simu_clusterSizeX.GetXaxis().SetTitle('cluster size [# of pixels]')
	#clusterSizeX.GetYaxis().SetTitle('count')
	simu_clusterSizeX.SetLineWidth(2)
	simu_clusterSizeX.SetMarkerSize(0)
	simu_clusterSizeX.SetLineColor(1)
	simu_clusterSizeX.SetMarkerColor(1)
	data_clusterSizeX.SetLineWidth(2)
	data_clusterSizeX.SetMarkerSize(0)
	data_clusterSizeX.SetLineColor(2)
	data_clusterSizeX.SetMarkerColor(2)
	c = ROOT.TCanvas()
	legend = ROOT.TLegend(0.60, 0.7, 0.75, 0.8)
	legend.SetTextFont(62)
	legend.SetTextSize(0.03)
	legend.SetBorderSize(0)
	legend.SetFillStyle(0)
	legend.SetTextAlign(12)
	simu_clusterSizeX.Draw("hist")
	data_clusterSizeX.Draw("samehist")
	legend.AddEntry(simu_clusterSizeX, "MC, mean = %.2f \mum"%simu_clusterSizeX.GetMean(), "l")
	legend.AddEntry(data_clusterSizeX, "data, mean = %.2f \mum"%data_clusterSizeX.GetMean(), "l")
	legend.Draw("same")
	pt1 = ROOT.TPaveText(0.15, 0.78, 0.25, 0.88, "brNDC")
	pt1.SetFillStyle(0)
	pt1.SetBorderSize(0)
	pt1.SetTextAlign(12)
	pt1.SetTextSize(0.03)
	pt1.AddText("Proton 120 GeV")
	pt1.AddText("Tel{}".format(telCorr))
	pt1.Draw("same")
	c.SaveAs('plots/clusterSize/comp_dut{}-tel{}-cluster.pdf'.format(telCorr, telCorr))
	c.SaveAs('plots/clusterSize/comp_dut{}-tel{}-cluster.C'.format(telCorr, telCorr))




if __name__ == '__main__':
    plot(int(sys.argv[1]))
